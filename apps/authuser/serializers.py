from django.contrib.auth import authenticate, get_user_model
from rest_framework import serializers
from django.utils.translation import gettext_lazy as _
from apps.loan.models import Loan
from .models import CustomUser
from rest_framework.authtoken.models import Token

User = get_user_model()


class AuthTokenSerializer(serializers.Serializer):
    username = serializers.CharField(label=_('username'), write_only=True)
    password = serializers.CharField(label=_('password'), write_only=True)

    def validate(self, attrs):
        username = attrs.get('username')
        password = attrs.get('password')
        if username and password:
            user = authenticate(request=self.context.get('request'), username=username, password=password)

            if not user:
                msg = _('unable to login with given credential')
                raise serializers.ValidationError(msg, code='authentication')
        else:
            msg = _('Must include username and password')
            raise serializers.ValidationError(msg, code='authentication')
        attrs['user'] = user
        return attrs

class UserSerializer(serializers.ModelSerializer):
    token = serializers.SerializerMethodField()
    
    def get_token(self, obj):
        instance, created = Token.objects.get_or_create(user=obj)
        return instance.key


    class Meta:
        model = CustomUser
        fields = ('id', 'username', 'token', 'first_name', 'last_name', 'email', 'user_type', 'address', 'contact', 'gender', 'date_of_birth')

class UserCreateSerializer(UserSerializer):
    class Meta:
        model = CustomUser
        fields = ('username', 'first_name', 'last_name', 'email', 'address', 'contact', 'gender', 'date_of_birth', 'password')
        extra_kwargs = {
            'first_name': { 'required' : True},
            'last_name': { 'required' : True},
            'email': { 'required' : True},
        }



class CustomerCreateSerializer(serializers.Serializer):
    from apps.loan.serializers import LoanSerializer
    user = UserCreateSerializer()
    loan = LoanSerializer()

    def create(self, validated_data):
        loan = validated_data.pop('loan') if validated_data.get('loan') else None
        password = validated_data.get('user').pop('password') if validated_data.get('user').get('password') else None
        user = User.objects.create(is_active=False, user_type=CustomUser.NEW_CUSTOMER, **validated_data.get('user'))
        if password:
            user.set_password(password)
            user.save()
        if loan:
            Loan.objects.create(user=user, **loan)
        return user