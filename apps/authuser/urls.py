from django.urls import include, path
from rest_framework import routers
from .api_views import LoginView, CustomerCreateView, NewCustomerListAPIView

router = routers.DefaultRouter()


urlpatterns = [
    path('', include(router.urls)),
    path('api-login/', LoginView.as_view(), name='login'),
    path('customer-signup/', CustomerCreateView.as_view(), name='customer_signup'),
    path('customer-list/', NewCustomerListAPIView.as_view(), name='customer_list'),
]