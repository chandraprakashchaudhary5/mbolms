from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import CustomUser, NewCustomerModel, RegisteredCustomerModel
from django.utils.translation import gettext, gettext_lazy as _

class CustomUserAdmin(UserAdmin):
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'email', 'user_type', 'gender', 'contact', 'address', 'date_of_birth')}),
        (_('Permissions'), {
            'fields': ('is_active', 'is_staff'),
        }),
    )

    def save_model(self, request, obj, form, change):
        if obj.is_active:
            print(obj.is_active)
            obj.user_type = CustomUser.REGISTERED_CUSTOMER
        return super().save_model(request, obj, form, change)

class NewCustomerAdmin(UserAdmin):
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'email', 'user_type', 'gender', 'contact', 'address', 'date_of_birth')}),
        (_('Permissions'), {
            'fields': ('is_active', 'is_staff'),
        }),
    )

    def save_model(self, request, obj, form, change):
        if obj.is_active:
            print(obj.is_active)
            obj.user_type = CustomUser.REGISTERED_CUSTOMER
        return super().save_model(request, obj, form, change)

class RegisteredCustomerAdmin(CustomUserAdmin):
    pass

admin.site.register(CustomUser, CustomUserAdmin)
admin.site.register(NewCustomerModel, NewCustomerAdmin)
admin.site.register(RegisteredCustomerModel, RegisteredCustomerAdmin)
