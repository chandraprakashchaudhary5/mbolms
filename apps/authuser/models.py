from django.db import models
from django.contrib.auth.models import AbstractUser, UserManager
# from django.db.models.manager import Manager

class CustomUser(AbstractUser):
    ADMIN, NEW_CUSTOMER, REGISTERED_CUSTOMER = 'Admin', 'New Customer', 'Registered Customer'
    USER_TYPES = [(c,c) for c in [ADMIN, NEW_CUSTOMER, REGISTERED_CUSTOMER]]
    MALE, FEMALE, OTHER = 'Male', 'Female', 'Other'
    GENDER_CHOICES = [(c,c) for c in [MALE, FEMALE, OTHER]]
    user_type = models.CharField(max_length=100, choices=USER_TYPES, null=True)
    address = models.CharField(max_length=200, null=True)
    contact = models.CharField(max_length=200, null=True)
    gender = models.CharField(choices=GENDER_CHOICES, max_length=50, null=True)
    date_of_birth = models.DateField(null=True)

    def get_full_name(self) -> str:
        return super().get_full_name()


class NewCustomerManager(UserManager):
    def get_queryset(self):
        return super().get_queryset().filter(user_type=CustomUser.NEW_CUSTOMER)


class NewCustomerModel(CustomUser):
    objects = NewCustomerManager()

    class Meta:
        proxy = True
        verbose_name = 'New Customer'
        verbose_name_plural = 'New Customer'


class RegisteredCustomerManager(UserManager):
    def get_queryset(self):
        return super().get_queryset().filter(user_type=CustomUser.REGISTERED_CUSTOMER)


class RegisteredCustomerModel(CustomUser):
    objects = RegisteredCustomerManager()

    class Meta:
        proxy = True
        verbose_name = 'Registered Customer'
        verbose_name_plural = 'Registered Customer'