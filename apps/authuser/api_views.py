from rest_framework import serializers
from rest_framework.views import APIView
from django.contrib.auth import get_user_model
from .serializers import AuthTokenSerializer, UserSerializer, CustomerCreateSerializer
from rest_framework.response import Response
from rest_framework.generics import CreateAPIView, ListAPIView

User = get_user_model()


class LoginView(CreateAPIView):
    """
        This api is used to login
    """
    queryset = User.objects.all()
    serializer_class = AuthTokenSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data= request.data, context={'request':request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        serialized_profile = UserSerializer(user, context= {'request': request})
        return Response(serialized_profile.data)

class CustomerCreateView(CreateAPIView):
    """
        This api is used to register new customer
    """
    queryset = User.objects.all()
    serializer_class = CustomerCreateSerializer

    def post(self, request, *args, **kwargs):
        serializer = CustomerCreateSerializer(data=request.data, context={'request': request})
        if serializer.is_valid(raise_exception=True):
            user = serializer.save()
            return Response(UserSerializer(user, context= {'request': request}).data)


class NewCustomerListAPIView(ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    filter_field = ('user_type', )
