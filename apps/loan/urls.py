from django.urls import include, path
from rest_framework import routers
from .api_views import LoanAPIView

router = routers.DefaultRouter()


urlpatterns = [
    path('', include(router.urls)),
    path('loan/', LoanAPIView.as_view(), name='loan'),
]