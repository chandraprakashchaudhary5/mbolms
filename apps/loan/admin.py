from django.contrib import admin
from .models import Loan

class LoanAdmin(admin.ModelAdmin):
    list_display = ('loan_id', 'loan_type', 'loan_terms', 'instalment_amount')
    # readonly_fields = ('loan_id', )
    list_display_links = ('loan_id', 'loan_type')

admin.site.register(Loan, LoanAdmin)
