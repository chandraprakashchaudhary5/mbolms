from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver
from .models import Loan
import random
import string


@receiver(pre_save, sender=Loan)
def assign_unique_id_on_loan_approved(sender, instance, *args, **kwargs):
    try:
        
        previous = Loan.objects.filter(id=instance.id).first()
        if previous.status != instance.status and instance.status == Loan.APPROVED:
            slug = ''.join(random.choices(string.ascii_uppercase, k=5))
            while Loan.objects.filter(loan_id=slug).exists():
                slug = ''.join(random.choices(string.ascii_uppercase, k=5))
            instance.loan_id = slug
            instance.instalment_amount_per_month = instance.instalment_amount_for_each_month()
            instance.save()
    except:
        pass

@receiver(post_save, sender=Loan)
def remaining_amount_property(sender, instance, created, *args, **kwargs):
    print('called')
    if created:
        instance.remaining_amount = instance.instalment_amount
        instance.save()