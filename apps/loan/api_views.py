from .models import Loan
from rest_framework.permissions import IsAuthenticated
from rest_framework.generics import ListCreateAPIView
from rest_framework.response import Response
from .serializers import LoanSerializer

class LoanAPIView(ListCreateAPIView):
    queryset = Loan.objects.all()
    serializer_class = LoanSerializer
    permission_classes = (IsAuthenticated,)
    filter_fields = ('loan_type', )

    def get_queryset(self):
        return Loan.objects.filter(user=self.request.user)

    def post(self, request, *args, **kwargs):
        serializer = LoanSerializer(data=request.data, context={'request': request})
        if serializer.is_valid(raise_exception=True):
            serializer.save(user=request.user)
            return Response(serializer.data)