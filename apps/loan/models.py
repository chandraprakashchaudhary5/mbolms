from django.db import models
from django.contrib.auth import get_user_model

User = get_user_model()

class Loan(models.Model):
    EL, CL, HL, PL = 'EL', 'CL', 'HL', 'PL'
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    LOAN_TYPE = [(c,c) for c in [EL, CL, HL, PL]]
    PENDING, APPROVED, PAID = 'Pending', 'Approved', 'Paid'
    STATUS = [(c,c) for c in [PENDING, APPROVED, PAID]]
    loan_type = models.CharField(choices=LOAN_TYPE, max_length=50)
    loan_terms = models.IntegerField(help_text='In years')
    instalment_amount = models.FloatField()
    loan_id = models.CharField(max_length=100, blank=True)
    instalment_amount_per_month = models.FloatField(null=True, blank=True)
    remaining_amount = models.FloatField(null=True, blank=True)
    instalment_date = models.DateField(null=True, blank=True)
    status = models.CharField(choices=STATUS, max_length=100, default=PENDING)

    def __str__(self) -> str:
        return 'loan of user: %s type: %s' %(self.user, self.loan_type)

    def instalment_amount_for_each_month(self):
        return round(self.instalment_amount/ (self.loan_terms * 12), 2)
    
    @property
    def paid_loan(self):
        from django.db.models import Sum
        qs = self.transaction_set.all()
        amount = qs.aggregate(paid_amount=Sum('amount')).get('paid_amount') or 0
        return amount
    
    def update_remaining_amount(self):
        remaining_amount = self.instalment_amount
        if self.paid_loan:
            remaining_amount = self.instalment_amount - self.paid_loan
        self.remaining_amount = remaining_amount
        self.save()
    
    def check_payment_complete(self):
        return self.remaining_amount <= 0

