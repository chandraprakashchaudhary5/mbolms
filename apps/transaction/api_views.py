import rest_framework
from rest_framework import serializers
from rest_framework.generics import ListCreateAPIView
from rest_framework.response import Response
from .models import Transaction
from .Serializers import TransactionSerializer
from rest_framework.permissions import IsAuthenticated

class TransactionAPIView(ListCreateAPIView):
    queryset = Transaction.objects.all()
    serializer_class = TransactionSerializer
    permission_classes = (IsAuthenticated,)
    filter_fields = ('loan', )

    def get_queryset(self):
        return Transaction.objects.filter(loan__user=self.request.user)

    def post(self, request, *args, **kwargs):
        serializer = TransactionSerializer(data=request.data, context={'request': request})
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            return Response(serializer.data)