from django.urls import include, path
from rest_framework import routers
from .api_views import TransactionAPIView

router = routers.DefaultRouter()


urlpatterns = [
    path('', include(router.urls)),
    path('transaction/', TransactionAPIView.as_view(), name='transaction'),
]