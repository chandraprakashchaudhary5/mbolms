from django.db import models

from apps.loan.models import Loan

class Transaction(models.Model):
    loan = models.ForeignKey(Loan, on_delete=models.CASCADE)
    amount = models.FloatField(null=True)
    description = models.TextField()
    datetime_creataed = models.DateTimeField(auto_now_add=True)

    def __str__(self) -> str:
        return 'Transaction for loan: %s for date: %s '%(self.loan, self.datetime_creataed)
