from django.core.exceptions import ValidationError
from django.db.models import fields
from rest_framework import serializers
from .models import Transaction
from apps.loan.models import Loan

class TransactionSerializer(serializers.ModelSerializer):
    def validate(self, attrs):
        loan = attrs.get('loan')
        print(loan)
        if loan.status != Loan.APPROVED:
            raise ValidationError({'loan': 'Loan status must be approve for transaction.'})

        return super().validate(attrs)
    class Meta:
        model = Transaction
        fields = '__all__'