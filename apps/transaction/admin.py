from django.contrib import admin
from .models import Transaction


class TransactionAdmin(admin.ModelAdmin):
    list_display = ('loan', 'amount', 'description', 'datetime_creataed')
    list_filter = ('loan__user', 'loan__loan_type', 'datetime_creataed')


admin.site.register(Transaction, TransactionAdmin)
