from django.dispatch import receiver
from django.db.models.signals import post_save

from apps.loan.models import Loan
from .models import Transaction

@receiver(post_save, sender=Transaction)
def substract_paid_amount_from_loan(sender, instance, created, *args, **kwargs):
    if created:
        # print('signal')
        instance.loan.update_remaining_amount()
        if instance.loan.check_payment_complete():
            instance.loan.status = Loan.PAID
        instance.loan.save()
